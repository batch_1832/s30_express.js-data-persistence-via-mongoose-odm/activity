const express = require ("express");
const mongoose = require ("mongoose");
const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.bcygx.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("Where connected to the cloud database!."));

// 1. Create a User schema.
const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    status: {
        type: String,
        default: "Pending"
    }
});


// 2. Create a User model.

const User = mongoose.model("User", userSchema);

// 3. Create a POST route that will access the "/signup" route that will create a user.

app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username}, (err, result) =>{
        
        if(result != null && result.username == req.body.username){
            return res.send("Duplicate username found!")
        }
        
        else{
            if(req.body.username != "" && req.body.password != ""){
                let newUser = new User({
                    username: req.body.username,
                    password: req.body.password
                });

                newUser.save((saveErr, saveUser) =>{
                    if(saveErr){
                        return console.error(saveErr);
                    }
                    else{
                        return res.status(200).send("New User Registered!");
                    }
                })
            }
            else{
                return res.send("Both username and password must be provided.");
            }
        }
    })
})



// 4. Process a POST request at the "/signup" route using postman to register a user.


// 5. Create a git repository named S30.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.



app.listen(port, () => console.log(`Server running at port${port}`));



